function myMap(elements, cb) {

    let array=[];

    if (!Array.isArray(elements)){
        return ('Invalid input, please input array');
    }

    for (let index = 0; index < elements.length; index++){
        array.push(cb(elements[index]));
    }
    return array;
}
module.exports={myMap};